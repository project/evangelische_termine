/* Javascript document */
(function ($, Drupal) {

    $(document).ready(function () {

        $('.colorbox-container').on('click','.colorbox-load', function(){

            $.colorbox({
                iframe: true,
                href:$(this).attr('href'),
                innerWidth:"769px",
                innerHeight:"80%",
                maxWidth:'100%'

            });
            return false;
        });
    });

})(jQuery, Drupal);

function increasepageId(){
    jQuery('#pageID').val(Number(jQuery('#pageID').val()) + 1);
}
function resetpageId(){
    jQuery('#pageID').val(2);
    var empty = jQuery('<div id="etmore-ajax-wrapper"></div>');
    jQuery('#etmore-clear_wrapper').html(empty);
}
function showMore(){
    jQuery('#etresult-more-wrapper').show();
}
function hideMore(){
    jQuery('#etresult-more-wrapper').hide();
}