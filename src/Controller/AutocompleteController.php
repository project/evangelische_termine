<?php

/**
 * @file
 * Contains \Drupal\et_slider\Controller\AutocompleteController.
 */

namespace Drupal\evangelische_termine\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Unicode;


class AutocompleteController extends ControllerBase {

    /**
     * Autocomplete-Funktion für die Suche nach Veranstaltern in der Listenansicht
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $field_name
     * @param string $type
     * @param string $typeid
     * @param string $host
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function handleAutocomplete(Request $request, $field_name, $type = 'D', $typeid = '404', $host = 'www.evangelische-termine.de'){

        $results = [];

        // Get the typed string from the URL, if it exists.
        if ($input = $request->query->get('q')) {

            $typed_string = Tags::explode($input);
            $typed_string = mb_strtolower(array_pop($typed_string));

            if($field_name == 'vid'){
                $results = json_decode($this->searchUser($typed_string, $host, $type, $typeid ));
            }


        }

        return new JsonResponse($results);

    }

    /**
     * Sucht nach Veranstaltern in der Veranstaltungsdatenbank
     * Verwendet von der Autocomplete-funktion
     * @param $q Suchwort
     * @param string $host Domain der Veranstaltungsdatenbank
     * @param $type D|K Dekanat oder Kirchenkreis
     * @param $id Dekanats- oder Kirchenkreisnummer
     * @return mixed|string Ergebnis
     */
    private function searchUser($q, $host = 'www.evangelische-termine.de', $type, $id){

        if(function_exists('curl_init')){

            $url = 'https://' . $host . '/searchuser/' . $q . '/' . $type . '/' . $id;
            $sobl = curl_init($url);
            curl_setopt($sobl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($sobl, CURLOPT_USERAGENT, 'Drupal 8 Module et_slider');
            curl_setopt($sobl, CURLOPT_REFERER, $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
            # timeout max 10 Sek.
            curl_setopt($sobl, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($sobl, CURLOPT_FOLLOWLOCATION, true);

            $pageContent = curl_exec ($sobl);
            $sobl_info = curl_getinfo ( $sobl);

            $ret = '';

            if($sobl_info['http_code'] == '200'){
                 $ret = $pageContent;
            } else {
                # Fehlermeldung:
                $ret = "Der Terminkalender ist derzeit nicht erreichbar.";
            }
        } else {

            $ret = 'Bitte installieren Sie das PHP curl Modul.';
        }
        return $ret;
    }
}
