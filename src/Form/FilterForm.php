<?php

namespace Drupal\evangelische_termine\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class FilterForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'et_filter_form';
  }

  var $params = array('eventtype','people','et_q','highlight','namevid','date');

  var $config = null;

  var $themefunction = 'result_etfilteredlist';





  /**
   * {@inheritdoc}.
   */
    public function buildForm(array $form, FormStateInterface $form_state, $config = null) {

        $this->config = $config;

        # später besser direkt von ET abrufen in Settings
        $eventtypes = array(
            'all' => 'alle',
            '1' => 'Gottesdienste',
            '2' => 'Gruppen / Kreise',
            '3' => 'Fortbildungen / Seminare / Vorträge',
            '4' => 'Konzerte / Theater / Musik',
            '5' => 'Freizeiten / Reisen',
            '6' => 'Ausstellungen / Kunst',
            '10' => 'Meditation / spirituelle Angebote',
            '7' => 'Feste / Feiern',
            '8' => 'Sport/Spiel',
            '9' => 'Sonstiges',
          '-1' => 'alle, außer Gottesdienste',
          '-2' => 'alle, außer Gruppen / Kreise',
          '-3' => 'alle, außer Fortbildungen / Seminare / Vorträge',
          '-4' => 'alle, außer Konzerte / Theater / Musik',
          '-5' => 'alle, außer Freizeiten / Reisen',
          '-6' => 'alle, außer Ausstellungen / Kunst',
          '-10' => 'alle, außer Meditation / spirituelle Angebote',
          '-7' => 'alle, außer Feste / Feiern',
          '-8' => 'alle, außer Sport/Spiel',
          '-9' => 'alle, außer Sonstiges'
        );
        $peopleArr = array(
            'all' => 'Alle',
            '5' => 'Kinder',
            '10' => 'Jugendliche',
            '15' => 'Junge Erwachsene',
            '16' => 'Frauen',
            '17' => 'Männer',
            '20' => 'Familien',
            '25' => 'Erwachsene',
            '30' => 'Senioren',
            '35' => 'besondere Zielgruppe'
        );

        $highlightArr = array(
          'low' => 'alle Veranstaltungen',
            'high' => 'nur Highlights'
        );


        /* Veranstaltungsn laden */
        $ajax = [
          'wrapper' => 'etresult-ajax-wrapper',
          'callback' => '::ajaxRebuildResult',
          'effect' => 'fade'
        ];


        // Form constructor

        $form['#tree'] = false;

        # Volltextsuche
        $form['et_q'] = array(
          '#type' => 'textfield',
          '#name' => 'et_q',
          '#default_value' => $config['et_q'],
          '#size' => 30,
          '#maxlength' => 30,
          '#attributes' => array('placeholder' => t('Suchbegriff'))
        );
        $form['search'] = array(
          '#type' => 'button',
          '#default_value' => t('Suchen'),
          '#ajax' => $ajax + ['trigger_as' => ['name' => 'etform-submit']]
        );

        $form['advanced'] = array(
          '#type' => 'details',
          '#title' => t('Erweiterter Filter'),
          '#open' => FALSE
        );

        $form['advanced']['highlight'] = array(
          '#type' => 'select',
          '#name' => 'highlight',
          '#ajax' => $ajax + ['trigger_as' => ['name' => 'etform-submit']],
          '#title' => $this->t('Auswahl'),
          '#default_value' => $this->config['highlight'],
          '#options' => $highlightArr
        );

        # Autocomplete Veranstalter bei Dekanat und Kirchenkreis
        if($config['et_displaytype'] != 'V'){

            $host = $this->getHost();

            $form['advanced']['namevid'] = array(
              '#type' => 'textfield',
              '#title' => $this->t('Kirchengemeinde / Einrichtung'),
              '#autocomplete_route_name' => 'evangelische_termine.autocomplete',
              '#autocomplete_route_parameters' => array('field_name' => 'vid','type' => $config['et_displaytype'],'typeid' => $config['typeid'] , 'host' => $host),
            );
            $form['advanced']['vidsearch'] = array(
              '#type' => 'button',
              '#default_value' => t('Los'),
              '#ajax' => $ajax + ['trigger_as' => ['name' => 'etform-submit']]
            );
        }




        $form['advanced']['eventtype'] = array(
          '#type' => 'select',
          '#name' => 'eventtype',
          '#ajax' => $ajax + ['trigger_as' => ['name' => 'etform-submit']],
          '#title' => $this->t('Kategorie'),
          '#default_value' => $this->config['eventtype'],
          '#options' => $eventtypes,

        );
        $form['advanced']['people'] = array(
          '#type' => 'select',
          '#name' => 'people',
          '#ajax' => $ajax + ['trigger_as' => ['name' => 'etform-submit']],
          '#title' => $this->t('Zielgruppe'),
          '#default_value' => $this->config['people'],
          '#options' => $peopleArr
        );
        $form['advanced']['date'] = array(
          '#type' => 'date',
          '#date_date_format' => 'd.m.Y',
          '#name' => 'date',
          '#title' => $this->t('Datum'),
          '#size' => 12,
          '#maxlength' => 10
        );
        $form['advanced']['searchdate'] = array(
          '#type' => 'button',
          '#default_value' => t('Los'),
          '#ajax' => $ajax + ['trigger_as' => ['name' => 'etform-submit']]
        );

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#name' => 'etform-submit',
          '#ajax' => $ajax,
          '#attributes' => array('class' => array('visually-hidden')),
          '#value' => $this->t('Filter anwenden'),
          '#button_type' => 'primary'
        );



        return $form;


    }


    /**
    * {@inheritdoc}.
    */
    public function validateForm(array &$form, FormStateInterface $form_state) {

        $sessionstore = \Drupal::service('tempstore.private')->get('et_filteredlist' . $this->config['instance_id']);
        foreach($this->params as $param){
            if($param == 'namevid'){
                $namevid = $form_state->getValue($param);
                $sessionstore->set($param,$this->extractVid($namevid));
            } else {
                $sessionstore->set($param, $form_state->getValue($param));

            }

        }

        $trigger = $form_state->getTriggeringElement();
        $sessionstore->set('trigger',$trigger['#name']);

    }

    private function extractVid($value){
        $p = '/\\(([0-9]+)\\)/';
        preg_match_all($p, $value, $matches);
        return $matches[1][0];

    }

    /**
     * {@inheritdoc}.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
    }

    /**
     * {@inheritdoc}.
     * array &$form, FormStateInterface $form_state
     */
    public function ajaxRebuildResult(){

        $sessionstore = \Drupal::service('tempstore.private')->get('et_filteredlist' . $this->config['instance_id']);

        # Bei mehr anzeigen, pageID aus der Session nehmen, sonst zurücksetzen
        if($sessionstore->get('trigger') == 'etform-more'){
            $pageID = $sessionstore->get('pageID');
        } else {
            $pageID = 1;
        }


        # Bei Neuaufruf der Seite, Werte aus Config
        if($sessionstore->get('trigger') == ''){
            $sessionstore->set('date' , '');
            foreach($this->params as $param){
                if($param == 'namevid' || $param == 'date') continue;
                $sessionstore->set($param , $this->config[$param]);
            }
        }

        # Werte aus Session
        foreach($this->params as $param){
            if($param == 'namevid') continue;
            if($param == 'date'){
                $urlParams[] =  'mysqldate=' . $sessionstore->get($param);
            } else {
                $urlParams[] = $param . '=' . $sessionstore->get($param);
            }

        }


        if($this->config['et_displaytype'] == 'V'){
            $urlParams[] = 'vid=' . $this->config['typeid'];
        }
        if($this->config['et_displaytype'] == 'D'){
            $urlParams[] = 'region=' . $this->config['typeid'];
        }
        if($this->config['et_displaytype'] == 'K'){
            $urlParams[] = 'kk=' . $this->config['typeid'];
        }
        # Bei Dekanat und Kirchenkreis und ausgewähltem Veranstalter:
        if($this->config['et_displaytype'] != 'V' && $sessionstore->get('namevid') != '' && $sessionstore->get('trigger') != ''){

            $urlParams[] = 'vid=' . $sessionstore->get('namevid');
        }

        $urlParams[] = 'cha=' . $this->config['cha'];
        $urlParams[] = 'place=' . $this->config['place'];
        $urlParams[] = 'person=' . $this->config['person'];
        $urlParams[] = 'ipm=' . $this->config['ipm'];

        $urlParams[] = 'itemsPerPage=' . $this->config['itemsPerPage'];
        $urlParams[] = 'pageID=' . $pageID;
        $urlParams[] = 'encoding=utf8';
        $urlParams[] = $this->config['addparams'];

        $queryString = implode('&',$urlParams);
        $json = $this->fetchJson($queryString);

        $config = $this->config;
        $config['et_host'] == '' ? $host = 'www.evangelische-termine.de' : $host = $config['et_host'];

        $img = "https://" . $host . '/bundles/vket/images/placeholder.png';

        $addoutput = '';
        $templateaddoutput = $this->config['addoutput']['value'];

        for($i = 0; $i < count($json); $i++){

            if($templateaddoutput != ''){
                $addoutput = $templateaddoutput;

                // Leere Werte filtern, Bsp: {if:_event_TEXTBOX_1}Interpreten: _event_TEXTBOX_1{if:end}
                $p = "/\{if:([^}]+?)\}/";
                $result = preg_match_all($p,$addoutput,$res);
                foreach($res[1] as $item){
                    if($item == 'end') continue;
                    $p2 = "/(\{if:".$item."\})([^}]+?)(\{if:end\})/";
                    if($json[$i]->Veranstaltung->$item == ''){
                        $addoutput = preg_replace($p2,'',$addoutput);
                    } else {
                        $addoutput = preg_replace($p2,'$2',$addoutput);
                    }
                }
                // Werte ersetzen:
                foreach ($json[$i]->Veranstaltung as $key => $value) {
                    $addoutput = str_replace($key, nl2br($value), $addoutput);
                }
            }


            $json[$i]->Veranstaltung->img = $img;
            if($json[$i]->Veranstaltung->_place_IMAGE != ''){
                $json[$i]->Veranstaltung->img = $json[$i]->Veranstaltung->_place_IMAGE;
            }
            if($json[$i]->Veranstaltung->_event_IMAGE != ''){
                $json[$i]->Veranstaltung->img = $json[$i]->Veranstaltung->_event_IMAGE;
            }

            $json[$i]->Veranstaltung->img = $this->removeHttp($json[$i]->Veranstaltung->img );

            $json[$i]->Veranstaltung->_place_GLAT = str_replace(',','.',$json[$i]->Veranstaltung->_place_GLAT);
            $json[$i]->Veranstaltung->_place_GLONG = str_replace(',','.',$json[$i]->Veranstaltung->_place_GLONG);

            $json[$i]->Veranstaltung->addoutput = $addoutput;

        }


        $ret['#content'] = $json;
        $ret['#showImages'] = $this->config['showImages'];
        #var_dump($this->config['showImages']);

        # Wenn weniger als itemsPerPage, dann more verstecken
        if(count($json) < $config['itemsPerPage']){
            $jsscript = 'hideMore();';
        } else {
            $jsscript = 'showMore();';
        }


        $ret['#jsscript'] = $jsscript;

        $ret['#host'] = $host;
        $ret['#url'] = 'http://' . $host . '/json?' .$queryString;
        $ret['#theme'] = $this->themefunction;



        $ret['#attached'] = array(
          'library' =>  array(
            'evangelische_termine/colorbox',
            'colorbox/default',
            'evangelische_termine/filteredlist'
          ),
        );

        return $ret ;
    }


    private function removeHttp($url){
        if(substr($url,0,5) == 'https'){
            $url = str_replace('https:','',$url);
        } else if(substr($url,0,4) == 'http'){
            $url = str_replace('http:','',$url);
        }
        return $url;
    }

    public function fetchJson($queryString){
        $host = $this->getHost();

        $url = "https://" . $host . "/json?" . $queryString;

        if(function_exists('curl_init')){
            # use curl
            $sobl = curl_init($url);
            curl_setopt($sobl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($sobl, CURLOPT_USERAGENT, 'ETFilterFormScript');
            curl_setopt($sobl, CURLOPT_REFERER, $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
            # timeout max 10 Sek.
            curl_setopt($sobl, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($sobl, CURLOPT_FOLLOWLOCATION, true);

            $pageContent = curl_exec ($sobl);
            $sobl_info = curl_getinfo ( $sobl);


            if($sobl_info['http_code'] == '200'){
                $ret = $pageContent;
            } else {
                # Fehlermeldung:
                $ret = "Der Terminkalender ist derzeit nicht erreichbar.";
            }
        } else {

            $ret = 'Bitte installieren Sie das PHP curl Modul.';
        }

        return json_decode($ret);

    }

/*
    public function ajaxRebuildForm(array &$form, FormStateInterface $form_state){

    }
*/
    /**
     * {@inheritdoc}
     */
    public function getCacheMaxAge() {
        return 0;
    }

    private function getHost(){
        $this->config['et_host'] == '' ? $host = 'www.evangelische-termine.de' : $host = $this->config['et_host'];
        return $host;
    }
}
