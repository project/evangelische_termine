<?php

namespace Drupal\evangelische_termine\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\evangelische_termine\Form\FilterForm;


class MoreForm extends FilterForm {


    var $themefunction = 'resultmore_etfilteredlist';


    /**
     * {@inheritdoc}.
     */
    public function getFormId() {
        return 'et_more_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $config = null) {

        $this->config = $config;

        $ajax = [
          'wrapper' => 'etmore-ajax-wrapper',
          'callback' => '::ajaxRebuildResult',
          'effect' => 'fade'
        ];

        $form['pageID'] = array(
          '#type' => 'hidden',
          '#attributes' => array('id' => 'pageID'),
          '#default_value' => 2
        );

        $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#name' => 'etform-more',
          '#ajax' => $ajax,
          '#value' => $this->t('Weitere Termine anzeigen'),
          '#button_type' => 'primary'
        );

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {

    }

    public function validateForm(array &$form, FormStateInterface $form_state) {
        $sessionstore = \Drupal::service('tempstore.private')->get('et_filteredlist' . $this->config['instance_id']);
        $sessionstore->set('pageID', $form_state->getValue('pageID'));

        $trigger = $form_state->getTriggeringElement();
        $sessionstore->set('trigger',$trigger['#name']);
    }



}
