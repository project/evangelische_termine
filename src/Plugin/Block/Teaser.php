<?php

namespace Drupal\evangelische_termine\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Teaser' block.
 *
 * @Block(
 *  id = "et_listteaser",
 *  admin_label = @Translation("Evangelische-Termine Teaser"),
 * )
 */
class Teaser extends BlockBase implements BlockPluginInterface {

    private $kat = array(
      'all' => 'alle',
      '1' => 'Gottesdienste',
      '2' => 'Gruppen / Kreise',
      '3' => 'Fortbildungen / Seminare / Vorträge',
      '4' => 'Konzerte / Theater / Musik',
      '5' => 'Freizeiten / Reisen',
      '6' => 'Ausstellungen / Kunst',
      '10' => 'Meditation / spirituelle Angebote',
      '7' => 'Feste / Feiern',
      '8' => 'Sport/Spiel',
      '9' => 'Sonstiges',
      '-1' => 'alle, außer Gottesdienste',
      '-2' => 'alle, außer Gruppen / Kreise',
      '-3' => 'alle, außer Fortbildungen / Seminare / Vorträge',
      '-4' => 'alle, außer Konzerte / Theater / Musik',
      '-5' => 'alle, außer Freizeiten / Reisen',
      '-6' => 'alle, außer Ausstellungen / Kunst',
      '-10' => 'alle, außer Meditation / spirituelle Angebote',
      '-7' => 'alle, außer Feste / Feiern',
      '-8' => 'alle, außer Sport/Spiel',
      '-9' => 'alle, außer Sonstiges'
    );

    private $people = array(
      'all' => 'Alle',
      '5' => 'Kinder',
      '40' => 'Konfirmanden',
      '10' => 'Jugendliche',
      '15' => 'Junge Erwachsene',
      '16' => 'Frauen',
      '17' => 'Männer',
      '20' => 'Familien',
      '25' => 'Erwachsene',
      '30' => 'Senioren',
      '35' => 'besondere Zielgruppe'
    );



    /**
    * {@inheritdoc}
    */
    public function build() {


        $config = $this->getConfiguration();


        $querypart = '&count=' . $config['teaser_count']
        . '&highlight=' . $config['teaser_high']
        . '&eventtype=' . $config['teaser_kat']
        . '&people=' . $config['teaser_people']
        . '&et_q=' . urlencode($config['teaser_q'])
        . '&place=' . $config['teaser_placeid']
        . '&cha='. $config['teaser_cha']
        . '&encoding=utf8' ;

        $addparams = $config['teaser_addparams'];

        if($addparams != ''){
            if(substr($addparams,0,1) != '&' ){
                $querypart .= '&';
            }
            $querypart .=  $addparams;
        }

        switch($config['teaser_displaytype']) {
            case "V":
                $queryString = 'vid=' . $config['teaser_vid'] . $querypart;
                break;
            case "D":
                $queryString = 'vid=all&region=' . $config['teaser_vid'] . $querypart;
                break;
        }

        $config['teaser_host'] == '' ? $host = 'www.evangelische-termine.de' : $host = $config['teaser_host'];

        $url = "https://" . $host . "/json?" . $queryString;

        $content = $this->get_block_content($url);

        $img = "//" . $host . '/bundles/vket/images/placeholder.png';

        $addoutput = '';
        $templateaddoutput = $config['addoutput']['value'];

        $group = array();
        if(is_array($content)){
            foreach($content as $item){


                if($templateaddoutput != ''){
                    $addoutput = $templateaddoutput;

                    // Leere Werte filtern, Bsp: {if:_event_TEXTBOX_1}Interpreten: _event_TEXTBOX_1{if:end}
                    $p = "/\{if:([^}]+?)\}/";
                    $result = preg_match_all($p,$addoutput,$res);
                    foreach($res[1] as $resitem){
                        if($resitem == 'end') continue;
                        $p2 = "/(\{if:".$resitem."\})([^}]+?)(\{if:end\})/";
                        if($item->Veranstaltung->$resitem == ''){
                            $addoutput = preg_replace($p2,'',$addoutput);
                        } else {
                            $addoutput = preg_replace($p2,'$2',$addoutput);
                        }
                    }
                    // Werte ersetzen:
                    foreach ($item->Veranstaltung as $key => $value) {
                        $addoutput = str_replace($key, nl2br($value), $addoutput);
                    }

                    $item->Veranstaltung->addoutput = $addoutput;
                }


                $item->img = $img;
                if($item->Veranstaltung->_place_IMAGE != ''){
                    $item->img = $item->Veranstaltung->_place_IMAGE;
                }
                if($item->Veranstaltung->_event_IMAGE != ''){
                    $item->img = $item->Veranstaltung->_event_IMAGE;
                }

                $item->img = $this->removeHttp($item->img);

                $group[] = $item;

            }
        }


        return array(
          '#theme'=> 'et_listteaser',
          '#content' => $group,
          '#sliderid' => 'id' . substr( uniqid(),10, 8),
          '#showImages' => $config['showImages'],
          '#host' => $host,
          '#cache' => array(
            'max-age' => 300
          ),

          '#attached' => array(
            'library' =>  array(
              'evangelische_termine/colorbox',
              'colorbox/default',
              'evangelische_termine/listteaser'
            ),
          )
        );


    }

    private function removeHttp($url){
        if(substr($url,0,5) == 'https'){
            $url = str_replace('https:','',$url);
        } else if(substr($url,0,4) == 'http'){
            $url = str_replace('http:','',$url);
        }
        return $url;
    }
/*
    public function lazybuild(){

        $config = $this->getConfiguration();

        return array(
          '#theme'=> 'blockteaser',
          '#content' => array(
            '#lazy_builder' => ['evangelische_termine.listteaserloader:loader', $config ],
            '#create_placeholder' => TRUE
          ),

          '#cache' => array(
            'max-age' => 0,
            'tags' => ['et_listteaser'],
              #'contexts' => ['node']
          ),

          '#attached' => array(
            'library' =>  array(
              'evangelische_termine/colorbox',
              'colorbox/default',
              'evangelische_termine/listteaser'
            ),
          )
        );
    }
*/


    private function get_block_content ($url){
        #$config = $this->getConfiguration();

        if(function_exists('curl_init')){
            # use curl
            $sobl = curl_init($url);
            curl_setopt($sobl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($sobl, CURLOPT_USERAGENT, 'ListTeaserScript');
            curl_setopt($sobl, CURLOPT_REFERER, $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
            # timeout max 10 Sek.
            curl_setopt($sobl, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($sobl, CURLOPT_FOLLOWLOCATION, true);

            $pageContent = curl_exec ($sobl);
            $sobl_info = curl_getinfo ( $sobl);

            if($sobl_info['http_code'] == '200'){
                $ret =  $pageContent;
            } else {
                # Fehlermeldung:
                $ret = "Der Terminkalender ist derzeit nicht erreichbar.";
            }
        } else {

            $ret = 'Bitte installieren Sie das PHP curl Modul.';
        }

        return json_decode($ret);
    }



    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);

        $config = $this->getConfiguration();

        $form['teaser_displaytype'] = array(
          '#type' => 'select',
          '#title' => $this->t('Ausgabe-Version'),
          '#default_value' => isset($config['teaser_displaytype']) ? $config['teaser_displaytype'] : 'V',
          '#options' => array(
            'V' => 'Ausgabe Veranstalter',
            'D' => 'Ausgabe Dekanat'
          ),
          '#description' => $this->t('Ausgabe der Termine entweder für einen oder mehrere Veranstalter oder für ein Dekanat'),
        );



        $form['teaser_vid'] = array (
          '#type' => 'textfield',
          '#title' => $this->t('Veranstalter-ID / Dekanats-ID'),
          '#description' => $this->t('Eine oder mehrere durch Komma getrennte Veranstalter-IDs. Bei Ausgabe für Dekanat: 3-stellige Dekanatsnummer'),
          '#default_value' => isset($config['teaser_vid']) ? $config['teaser_vid'] : '3'
        );

        $form['teaser_count'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Anzahl von Veranstaltungen'),
          '#default_value' => isset($config['teaser_count']) ? $config['teaser_count'] : '5',
          '#size' => 2,
          '#maxlength' => 2,
          '#description' => $this->t("Anzahl der angezeigten Veranstaltungen (4 pro Slide).")
        );

        $form['teaser_q'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Suchwort'),
          '#default_value' => isset($config['teaser_q']) ? $config['teaser_q'] : '',
          '#size' => 25,
          '#maxlength' => 40,
          '#description' => $this->t("Suchwort")
        );
        $form['teaser_high'] = array(
          '#type' => 'radios',
          '#title' => $this->t('Nur Highlights oder alle Veranstaltungen'),
          '#default_value' => isset($config['teaser_high']) ? $config['teaser_high'] : 'all',
          '#options' => array('high' => t('Nur Highlights'), 'all' => t('alle Veranstaltungen'))
        );
        $form['teaser_kat'] = array(
          '#type' => 'select',
          '#title' => $this->t('Veranstaltungs-Kategorie'),
          '#default_value' => isset($config['teaser_kat']) ? $config['teaser_kat'] : 'all',
          '#options' => $this->kat,
          '#description' => $this->t('Veranstaltungskategorie als Filter der angezeigten Veranstaltungen.')
        );
        $form['teaser_people'] = array(
          '#type' => 'select',
          '#title' => $this->t('Zielgruppe'),
          '#default_value' => isset($config['teaser_people']) ? $config['teaser_people'] : '0',
          '#options' => $this->people,
          '#description' => $this->t('Zielgruppe als Filter der angezeigten Veranstaltungen.')
        );

        $form['teaser_placeid'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('ID des Veranstaltungsorts'),
          '#default_value' => isset($config['teaser_placeid']) ? $config['teaser_placeid'] : '',
          '#size' => 10,
          '#maxlength' => 60,
          '#description' => $this->t("Eine oder mehrere durch Komma getrennte Veranstaltungsort-IDs. Leerlassen für alle Veranstaltungsorte.")
        );
        $form['teaser_cha'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('ID des Kanals'),
          '#default_value' => isset($config['teaser_cha']) ? $config['teaser_cha'] : '',
          '#size' => 10,
          '#maxlength' => 60,
          '#description' => $this->t("Eine oder mehrere durch Komma getrennte Kanal-IDs. Leerlassen für alle Kanäle.")
        );


        $form['Erweitert'] = array(
          '#type' => 'details',
          '#title' => $this->t('Erweitert'),
          '#open' => false
        );
        $form['Erweitert']['teaser_addparams'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Zusätzliche Parameter'),
          '#default_value' => isset($config['teaser_addparams']) ? $config['teaser_addparams'] : '',
          '#size' => 30,
          '#maxlength' => 60,
          '#description' => $this->t("Optional zusätzliche Parameter. Siehe http://handbuch.evangelische-termine.de")
        );

        $form['Erweitert']['showImages'] = array(
          '#type' => 'select',
          '#title' => $this->t('Bilder ausblenden'),
          '#default_value' => isset($config['showImages']) ? $config['showImages'] : '0',
          '#options' => array(
            '0' => 'Bilder anzeigen',
            '1' => 'Bilder ausblenden'
          ),
          '#description' => $this->t('Grundsätzlich die Bilder der Veranstaltungen anzeigen oder ausblenden'),
        );

      $form['Erweitert']['addoutput'] = array(
        '#type' => 'text_format',
        '#format' => isset($config['addoutput']) ? $config['addoutput']['format'] : 'restricted_html',
        '#title' => $this->t('Zusätzliche Anzeige'),
        '#default_value' => isset($config['addoutput']) ? $config['addoutput']['value'] : '',
        '#size' => 15,
        '#description' => $this->t("Text und Platzhalter für zusätzliche Felder in der Liste. Bsp: {if:_event_LONG_DESCRIPTION}<strong>Interpreten:</strong> _event_LONG_DESCRIPTION{if:end}")
      );

        $form['Erweitert']['teaser_host'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Veranstaltungskalender URL'),
          '#default_value' => isset($config['teaser_host']) ? $config['teaser_host'] : '',
          '#size' => 40,
          '#description' => $this->t("Internet-Adresse des Veranstaltungskalenders (falls nicht www.evangelische-termine.de); Ohne 'http://'")
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        $this->setConfigurationValue('teaser_displaytype', $form_state->getValue('teaser_displaytype'));
        $this->setConfigurationValue('teaser_vid', $form_state->getValue('teaser_vid'));
        $this->setConfigurationValue('teaser_count', $form_state->getValue('teaser_count'));
        $this->setConfigurationValue('teaser_q', $form_state->getValue('teaser_q'));
        $this->setConfigurationValue('teaser_high', $form_state->getValue('teaser_high'));
        $this->setConfigurationValue('teaser_kat', $form_state->getValue('teaser_kat'));
        $this->setConfigurationValue('teaser_people', $form_state->getValue('teaser_people'));
        $this->setConfigurationValue('teaser_cha', $form_state->getValue('teaser_cha'));
        $this->setConfigurationValue('teaser_placeid', $form_state->getValue('teaser_placeid'));
        $this->setConfigurationValue('teaser_addparams', $form_state->getValue(array('Erweitert','teaser_addparams')));
        $this->setConfigurationValue('showImages', $form_state->getValue(array('Erweitert','showImages')));
        $this->setConfigurationValue('addoutput', $form_state->getValue(array('Erweitert','addoutput')));
        $this->setConfigurationValue('teaser_host', $form_state->getValue(array('Erweitert','teaser_host')));
    }
}
