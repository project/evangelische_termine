<?php

namespace Drupal\evangelische_termine\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\evangelische_termine\Form\FilterForm;

/**
 * Provides a 'FilteredList' block.
 *
 * @Block(
 *  id = "et_filteredlist",
 *  admin_label = @Translation("Evangelische-Termine Liste mit Filter"),
 * )
 */
class FilteredList extends BlockBase implements BlockPluginInterface {

    private $kat = array(
      'all' => 'alle',
      '1' => 'Gottesdienste',
      '2' => 'Gruppen / Kreise',
      '3' => 'Fortbildungen / Seminare / Vorträge',
      '4' => 'Konzerte / Theater / Musik',
      '5' => 'Freizeiten / Reisen',
      '6' => 'Ausstellungen / Kunst',
      '10' => 'Meditation / spirituelle Angebote',
      '7' => 'Feste / Feiern',
      '8' => 'Sport/Spiel',
      '9' => 'Sonstiges',
      '-1' => 'alle, außer Gottesdienste',
      '-2' => 'alle, außer Gruppen / Kreise',
      '-3' => 'alle, außer Fortbildungen / Seminare / Vorträge',
      '-4' => 'alle, außer Konzerte / Theater / Musik',
      '-5' => 'alle, außer Freizeiten / Reisen',
      '-6' => 'alle, außer Ausstellungen / Kunst',
      '-10' => 'alle, außer Meditation / spirituelle Angebote',
      '-7' => 'alle, außer Feste / Feiern',
      '-8' => 'alle, außer Sport/Spiel',
      '-9' => 'alle, außer Sonstiges'
    );

    private $people = array(
      'all' => 'Alle',
      '5' => 'Kinder',
      '40' => 'Konfirmanden',
      '10' => 'Jugendliche',
      '15' => 'Junge Erwachsene',
      '16' => 'Frauen',
      '17' => 'Männer',
      '20' => 'Familien',
      '25' => 'Erwachsene',
      '30' => 'Senioren',
      '35' => 'besondere Zielgruppe'
    );

  private $itemsPerPage = array(
    5 => 5,
    10 => 10,
    15 => 15,
    20 => 20,
    25 => 25
  );
    /**
     * {@inheritdoc}
     */
    public function build() {
        $config = $this->getConfiguration();

        $sessionmanager = \Drupal::service('session_manager');
        $currentuser = \Drupal::service('current_user');

        if ($currentuser->isAnonymous() && !isset($_SESSION['session_started'])) {
            $_SESSION['session_started'] = TRUE;
            $sessionmanager->start();
        }


        $sessionstore = \Drupal::service('tempstore.private')->get('et_filteredlist' . $config['instance_id']);
        $sessionstore->set('trigger','');

        $config['et_host'] == '' ? $host = 'www.evangelische-termine.de' : $host = $config['et_host'];


        $form = $this->getFilterForm($config);
        $moreform = \Drupal::formBuilder()->getForm('Drupal\evangelische_termine\Form\MoreForm', $config);

        # FilterForm-Instanz erstellen, um Inhalt abzurufen
        $filterform = new FilterForm();
        $filterform->config = $config;

        $content = $filterform->ajaxRebuildResult();



        return array(
          '#theme'=> 'et_filteredlist',
          '#content' => $content,
          '#showImages' => $config['showImages'],
          '#filteredlistid' => $config['instance_id'],
          '#host' => $host,
          '#form' => $form,
          '#moreform' => $moreform,
          '#cache' => array(
            'max-age' => 0,
          ),

          '#attached' => array(
            'library' =>  array(
              'evangelische_termine/colorbox',
              'colorbox/default',
              'evangelische_termine/filteredlist'
            ),
          )
        );

    }

    /**
     * Liefer das Formular für den Filter
     *
     * @param $config
     * @return mixed
     */
    private function getFilterForm($config){


        $form = \Drupal::formBuilder()->getForm('Drupal\evangelische_termine\Form\FilterForm', $config);
        return $form;
    }


    /**
     * Konfigurations-Formular
     *
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);

        $config = $this->getConfiguration();

        #var_dump($config);die();

        $form['et_displaytype'] = array(
          '#type' => 'select',
          '#title' => $this->t('Ausgabe-Version'),
          '#default_value' => isset($config['et_displaytype']) ? $config['et_displaytype'] : 'V',
          '#options' => array(
            'V' => 'Ausgabe Veranstalter',
            'D' => 'Ausgabe Dekanat',
            'K' => 'Ausgabe Kirchenkreis'
          ),
          '#description' => $this->t('Ausgabe der Termine entweder für einen oder mehrere Veranstalter, Dekanate oder Kirchenkreis.'),
        );



        $form['typeid'] = array (
          '#type' => 'textfield',
          '#title' => $this->t('Veranstalter-ID / Dekanats-ID / Kirchenkreis-ID'),
          '#description' => $this->t('Eine oder mehrere durch Komma getrennte Veranstalter-IDs. Bei Ausgabe für Dekanat: 3-stellige Dekanatsnummer. Bei Kirchenkreis: 1-stellig.'),
          '#default_value' => isset($config['typeid']) ? $config['typeid'] : '3'
        );



        $form['et_q'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Suchwort'),
          '#default_value' => isset($config['et_q']) ? $config['et_q'] : '',
          '#size' => 25,
          '#maxlength' => 40,
          '#description' => $this->t("Suchwort")
        );
        $form['highlight'] = array(
          '#type' => 'radios',
          '#title' => $this->t('Nur Highlights oder alle Veranstaltungen'),
          '#default_value' => isset($config['highlight']) ? $config['highlight'] : 'all',
          '#options' => array('high' => t('Nur Highlights'), 'all' => t('alle Veranstaltungen'))
        );
        $form['eventtype'] = array(
          '#type' => 'select',
          '#title' => $this->t('Veranstaltungs-Kategorie'),
          '#default_value' => isset($config['eventtype']) ? $config['eventtype'] : 'all',
          '#options' => $this->kat,
          '#description' => $this->t('Veranstaltungskategorie als Filter der angezeigten Veranstaltungen.')
        );
        $form['people'] = array(
          '#type' => 'select',
          '#title' => $this->t('Zielgruppe'),
          '#default_value' => isset($config['people']) ? $config['people'] : '0',
          '#options' => $this->people,
          '#description' => $this->t('Zielgruppe als Filter der angezeigten Veranstaltungen.')
        );

        $form['place'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('ID des Veranstaltungsorts (place)'),
          '#default_value' => isset($config['place']) ? $config['place'] : '',
          '#size' => 10,
          '#maxlength' => 60,
          '#description' => $this->t("Eine oder mehrere durch Komma getrennte Veranstaltungsort-IDs. Leerlassen für alle Veranstaltungsorte.")
        );
        $form['person'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('ID des Ansprechpartners (person)'),
          '#default_value' => isset($config['person']) ? $config['person'] : '',
          '#size' => 10,
          '#maxlength' => 60,
          '#description' => $this->t("Eine oder mehrere durch Komma getrennte Ansprechpartner-IDs. Leerlassen für alle Ansprechpartner.")
        );
        $form['cha'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('ID des Kanals (cha)'),
          '#default_value' => isset($config['cha']) ? $config['cha'] : '',
          '#size' => 10,
          '#maxlength' => 60,
          '#description' => $this->t("Eine oder mehrere durch Komma getrennte Kanal-IDs. Leerlassen für alle Kanäle.")
        );

        $form['ipm'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Veranstaltungstyp-ID (ipm)'),
          '#default_value' => isset($config['ipm']) ? $config['ipm'] : '',
          '#size' => 10,
          '#maxlength' => 60,
          '#description' => $this->t("Eine oder mehrere durch Komma IDs des Veranstaltungstyps. Leerlassen für alle Veranstaltungstypen.")
        );


        $form['Erweitert'] = array(
          '#type' => 'details',
          '#title' => $this->t('Erweitert'),
          '#open' => false
        );

        $form['Erweitert']['itemsPerPage'] = array(
          '#type' => 'select',
          '#title' => $this->t('Anzahl der Veranstaltungen pro Seite'),
          '#default_value' => isset($config['itemsPerPage']) ? $config['itemsPerPage'] : '20',
          '#options' => $this->itemsPerPage,

          '#description' => $this->t("Anzahl der Veranstaltungen, die auf der Seite angezeigt werden. Bei Klick auf 'Mehr anzeigen' wird jeweils nochmal maximal diese Anzahl an Veranstaltungen geholt.")
        );

        $form['Erweitert']['addparams'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Zusätzliche Parameter'),
          '#default_value' => isset($config['addparams']) ? $config['addparams'] : '',
          '#size' => 30,
          '#maxlength' => 60,
          '#description' => $this->t("Optional zusätzliche Parameter. Siehe http://handbuch.evangelische-termine.de")
        );

        $form['Erweitert']['showImages'] = array(
          '#type' => 'select',
          '#title' => $this->t('Bilder ausblenden'),
          '#default_value' => isset($config['showImages']) ? $config['showImages'] : '0',
          '#options' => array(
            '0' => 'Bilder anzeigen',
            '1' => 'Bilder ausblenden'
          ),
          '#description' => $this->t('Grundsätzlich die Bilder der Veranstaltungen anzeigen oder ausblenden'),
        );

        $form['Erweitert']['addoutput'] = array(
          '#type' => 'text_format',
          '#format' => isset($config['addoutput']) ? $config['addoutput']['format'] : 'restricted_html',
          '#title' => $this->t('Zusätzliche Anzeige'),
          '#default_value' => isset($config['addoutput']) ? $config['addoutput']['value'] : '',
          '#size' => 15,
          '#description' => $this->t("Text und Platzhalter für zusätzliche Felder in der Liste. Bsp: {if:_event_LONG_DESCRIPTION}<strong>Interpreten:</strong> _event_LONG_DESCRIPTION{if:end}")
        );

        $form['Erweitert']['et_host'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Veranstaltungskalender URL'),
          '#default_value' => isset($config['et_host']) ? $config['et_host'] : '',
          '#size' => 40,
          '#description' => $this->t("Internet-Adresse des Veranstaltungskalenders (falls nicht www.evangelische-termine.de); Ohne 'http://'")
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        $this->setConfigurationValue('eventtype', $form_state->getValue('eventtype'));
        $this->setConfigurationValue('people', $form_state->getValue('people'));
        $this->setConfigurationValue('et_q', $form_state->getValue('et_q'));
        $this->setConfigurationValue('highlight', $form_state->getValue('highlight'));


        $this->setConfigurationValue('cha', $this->clean($form_state->getValue('cha')));
        $this->setConfigurationValue('place', $this->clean($form_state->getValue('place')));
        $this->setConfigurationValue('person', $this->clean($form_state->getValue('person')));
        $this->setConfigurationValue('ipm', $this->clean($form_state->getValue('ipm')));


        $this->setConfigurationValue('et_displaytype', $form_state->getValue('et_displaytype'));
        $this->setConfigurationValue('typeid', $this->clean($form_state->getValue('typeid')));

        $this->setConfigurationValue('itemsPerPage', $form_state->getValue(array('Erweitert','itemsPerPage')));
        $this->setConfigurationValue('addparams', $form_state->getValue(array('Erweitert','addparams')));
        $this->setConfigurationValue('showImages', $form_state->getValue(array('Erweitert','showImages')));
        $this->setConfigurationValue('addoutput', $form_state->getValue(array('Erweitert','addoutput')));
        $this->setConfigurationValue('et_host', $form_state->getValue(array('Erweitert','et_host')));
        $this->setConfigurationValue('instance_id', 'id'.substr( uniqid(),4, 8));
    }

    private function clean($value){
        $value = str_replace(' ','',$value);
        return $value;
    }
}
