<?php
namespace Drupal\evangelische_termine\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Resource-Booking-Form' block.
 *
 * @Block(
 *  id = "et_resbooking",
 *  admin_label = @Translation("Evangelische-Termine Buchungsformular"),
 * )
 */
class ResBooking extends BlockBase implements BlockPluginInterface {


    /**
     * {@inheritdoc}
     */
    public function build() {

        $config = $this->getConfiguration();

        return array(
          '#theme' => 'resbooking',
          '#values' => $config,
          '#attached' => array(
            'library' =>  array(
              'evangelische_termine/et_rescalendar'
            ),
          )

        );
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);
        $config = $this->getConfiguration();

        $form['descr'] = array(
          '#markup' => "Informationen zum Buchungsformular finden Sie unter <a href=\"https://www.evangelische-termine.de/Admin/resourcelist\" target='_blank'>www.evangelische-termine.de/Admin/resourcelist</a>"
        );


        $form['vid'] = array(
          '#type' => 'textfield',
          '#title' => t('Veranstalter-ID'),
          '#description' => t('Geben Sie bitte die Veranstalter-ID von Evangelische-Termine.de ein'),
          '#default_value' => isset($config['vid']) ? $config['vid'] : '',
        );

        $form['res'] = array(
          '#type' => 'textfield',
          '#title' => t('Einschränken auf folgende Ressourcen:'),
          '#description' => t('Geben Sie bitte die Ressourcen-IDs von Evangelische-Termine.de ein, auf die die Darstellung begrenzt werden soll.'),
          '#default_value' => isset($config['res']) ? $config['res'] : '',
        );




        $form['grouped'] = array(
          '#type' => 'select',
          '#title' => t('Gruppierung'),
          '#default_value' => isset($config['grouped']) ? $config['grouped'] : '0',
          '#options' => array(
            '0' => 'Ohne Gruppierung',
            '1' => 'Gruppierung nach Schlagworten'
          )

        );

        $form['withblocked'] = array(
          '#type' => 'select',
          '#title' => t('Gesperrte Veranstaltungen'),
          '#default_value' => isset($config['withblocked']) ? $config['withblocked'] : '0',
          '#options' => array(
            '0' => 'gesperrte Veranstaltungen nicht anzeigen',
            '1' => 'auch gesperrte Veranstaltungen anzeigen'
          )

        );


        return $form;
    }


    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        $this->setConfigurationValue('vid', $form_state->getValue('vid'));
        $this->setConfigurationValue('res', $form_state->getValue('res'));
        $this->setConfigurationValue('grouped', $form_state->getValue('grouped'));
        $this->setConfigurationValue('withblocked', $form_state->getValue('withblocked'));

    }
}