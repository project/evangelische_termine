###### Einleitung

Das Modul evangelische_termine ist für die Integration des Online-Veranstaltungskalenders
evangelische-termine.de (und vergleichbarer Installationen) in Drupal 8 Seiten.

###### Features

Es bietet drei verschiedene Blöcke:
1. Veranstaltungsliste mit Suchfilter
2. Teaserblock
3. Ressourcenbuchungsformular in einem iFrame

Die Detailinformationen zu einer Veranstaltung werden in einem Colorbox-Popup angezeigt.

###### Requirements

Das Colorbox-Modul und die Colorbox-Library müssen installiert sein.
Vgl. https://www.drupal.org/project/colorbox

###### Maintainers

Vernetzte Kirche  
www.vernetzte-kirche.de
